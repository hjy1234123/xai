package cn.ydxiaoshuai.modules.conts;

/**
 * @author 小帅丶
 * @className RedisKey
 * @Description 一些业务的key
 * @Date 2020/9/21-16:03
 **/
public class RedisBizzKey {
    /**
     * 是否走接口检测图片
     **/
    public static String IS_CHECK_IMG = "IS_CHECK_IMG";
    /**
     * 是否走接口检测图片-是
     **/
    public static String IS_CHECK_IMG_YES = "0";
    /**
     * 是否走接口检测图片-否
     **/
    public static String IS_CHECK_IMG_NO = "1";
}

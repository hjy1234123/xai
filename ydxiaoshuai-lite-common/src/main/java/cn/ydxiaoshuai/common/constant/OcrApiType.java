package cn.ydxiaoshuai.common.constant;

/**
 * @author 小帅丶
 * @className OcrApiType
 * @Description 接口类型
 * @Date 2020/9/24-13:50
 **/
public enum OcrApiType {
    general,idcardb,idcardf,bank,handwriting;
}

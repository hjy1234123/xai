package cn.ydxiaoshuai.common.api.vo.linghitai;

import lombok.Data;

/**
 * @Description 人脸变老请求对象
 * @author 小帅丶
 * @className GrowOldRequestBean
 * @Date 2020/1/8-17:14
 **/
@Data
public class GrowOldRequestBean {
    //图片路径 暂时不用
    private String image_url;
    //图片base64字符串
    private String image_base64;
}

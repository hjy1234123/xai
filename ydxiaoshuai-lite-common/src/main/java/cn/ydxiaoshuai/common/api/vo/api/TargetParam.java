package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;

/**
 * @author 小帅丶
 * @className TargetBean
 * @Description 目标图信息
 * @Date 2020/5/29-14:17
 **/
@Data
public class TargetParam extends MergeBaseBean{
}

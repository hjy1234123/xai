package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;

import java.util.List;

/**
 * @author 小帅丶
 * @className EasyDLBDBean
 * @Description EasyDLBDBean
 * @Date 2020/3/12-11:30
 **/
@Data
public class EasyDLBDBean {

    private long log_id;
    private List<ResultsBean> results;

    @Data
    public static class ResultsBean {
        private double score;
        private String name;

    }
}

package cn.ydxiaoshuai.common.api.vo.palm;


import java.util.List;

/**
 * @Description 手相分析数据
 * @author 小帅丶
 * @className PalmDetainBean
 * @Date 2020/1/3-10:50
 **/
public class PalmDetainBean {

    /**
     * user_info : {"name":"","ucode":"","direct":0}
     * palm : {"isLeft":false,"hand":{"x":[157,425],"y":[28,494]},"palm":{"score":0.9871429800987244,"x":[0,190],"y":[190,466]},"fingers":{"middle":{"score":0.9997792840003967,"x":[74,121],"y":[1,57]},"ring":{"score":0.9911106824874878,"x":[40,83],"y":[26,82]},"index":{"score":0.9991061091423035,"x":[120,165],"y":[29,84]},"little":{"score":0.9992062449455261,"x":[2,46],"y":[93,147]}},"imgUrl":"https://bs-ai.ggwan.com/hand/product/7d3c4b748cbf42a3b4a5bf0ecd941565202001.jpg","lines":{"LL":{"x":[168,161,160,157,154,149,145,142,134,127,119,112,106,102,98,94,91,90,90,92],"y":[252,257,259,261,264,268,272,275,283,292,301,311,321,329,339,351,362,374,387,401]},"WL":{"x":[163,157,154,152,136,129,124,122,118,113,109,105,101,97,93,88,84,79,74,66],"y":[256,259,260,261,270,274,276,278,281,283,286,289,292,294,297,301,304,308,312,319]},"AL":{"x":[125,114,106,97,91,82,76,72,65,62,54,50,46,42,37,32,28,22,17,5],"y":[254,260,264,268,271,275,278,279,282,283,286,287,289,290,292,293,294,296,297,300]}},"handUrl":"https://bs-ai.ggwan.com/hand/product/7d3c4b748cbf42a3b4a5bf0ecd941565202001.jpg"}
     * shou_xiang : {"title":"手相分析","tag":"金形手","intro":"手相有阴阳男女之分，大小之别，手形之论，金木水火土是手形的基本分法。手形代表着人的吉凶富贵。","dec":["你的手形为金形手，金形手属于理性，工作能力强，有领导能力的手形，有此手形的男女，崇尚实际而不慕浮华，谨守秩序，意志坚定，忍耐力强，洁身自爱，能俭扑持家而不耽安逸，是务实派，能成大业且长寿，但欠圆滑，固执己见，并有过份吝啬的倾向，而无豪爽之气。 "],"sub_dec":[]}
     * ping_fen : {"LL_Score":81,"WL_Score":74,"AL_Score":84,"BL_Score":60,"fingerScore":92}
     * zhi_xiang : {"title":"指相分析","dec":[{"title":"有支配欲","dec":["你的食指比较长，代表你的性格也比较顽强，态度强硬，做事做人目的性很强，有理想有追求。","但在花钱方面可能会表现得很大方，财大气粗的，导致很难存得住钱，当然并不是说你的赚钱能力不强，只是赚钱多，花钱也多，需要加强理财，控制一下自己的欲望和消费习惯，才能存得住钱。"],"img":"https://img-fe.ggwan.com/images/82516ca217f7f0-164x163.png","name":"shi_zhi"},{"title":"运势较好","dec":["你的中指比较长，代表你的先天运况比较好，但也代表你会为人比较以自我为中心，做事作风全看自己的喜好，希望别人按照自己的想法来做事，给人很强势的感觉。","对与错区别在于你是帮人还是害人，所以，你需要多读书学习，提升自身的知识面和判断力，相信你能成为同事朋友眼中的军师高人，获得人缘助力。"],"img":"https://img-fe.ggwan.com/images/5dda8fbf8e8beb-164x163.png","name":"zhong_zhi"},{"title":"恋情不顺","img":"https://img-fe.ggwan.com/images/b2beb4f3a26a39-163x163.png","name":"wu_ming_zhi"},{"title":"子女惹祸","img":"https://img-fe.ggwan.com/images/d928215d854685-174x163.png","name":"xiao_zhi"}]}
     * zhang_wen_lei_xing : {"title":"掌纹类型分析"}
     * zhang_wen : {"title":"掌纹分析","dec":[{"title":"为情所累","img":"https://img-fe.ggwan.com/images/53289a1d5be3a1-170x163.png","name":"gan_qing"},{"title":"能力出众","img":"https://img-fe.ggwan.com/images/5da599c74baeef-171x165.png","name":"zhi_hui"},{"title":"不强求","img":"https://img-fe.ggwan.com/images/d3dcdca5573be7-170x163.png","name":"shi_ye"},{"title":"精力旺盛","img":"https://img-fe.ggwan.com/images/b0d6f320832726-170x163.png","name":"sheng_ming"}]}
     * score : {"title":"手相分数分析","score":88,"level":"很好"}
     * tag : {"title":"手相标签分析","dec":{"indexMiddle":{"score":5,"tag":"有支配欲"},"middle":{"score":5,"tag":"运势较好"},"ringMiddle":{"score":5,"tag":"恋情甜蜜"},"ringIndex":{"score":3,"tag":"恋情不顺"},"littleRing":{"score":3,"tag":"子女惹祸"},"hunYin":{"score":9,"tag":"异性缘少"},"zhiHui":{"score":18,"tag":"能力出众"},"shiYe":{"score":14,"tag":"不强求"},"shengMing":{"score":10,"tag":"精力旺盛"},"ganQing":{"score":17,"tag":"为情所累"},"zhangWen":{"score":8,"tag":"思维缜密"}}}
     */

    private UserInfoBean user_info;
    private String palm;
    private ShouXiangBean shou_xiang;
    private PingFenBean ping_fen;
    private ZhiXiangBean zhi_xiang;
    private ZhangWenLeiXingBean zhang_wen_lei_xing;
    private ZhangWenBean zhang_wen;
    private ScoreBean score;
    private TagBean tag;

    public UserInfoBean getUser_info() {
        return user_info;
    }

    public void setUser_info(UserInfoBean user_info) {
        this.user_info = user_info;
    }

    public String getPalm() {
        return palm;
    }

    public void setPalm(String palm) {
        this.palm = palm;
    }

    public ShouXiangBean getShou_xiang() {
        return shou_xiang;
    }

    public void setShou_xiang(ShouXiangBean shou_xiang) {
        this.shou_xiang = shou_xiang;
    }

    public PingFenBean getPing_fen() {
        return ping_fen;
    }

    public void setPing_fen(PingFenBean ping_fen) {
        this.ping_fen = ping_fen;
    }

    public ZhiXiangBean getZhi_xiang() {
        return zhi_xiang;
    }

    public void setZhi_xiang(ZhiXiangBean zhi_xiang) {
        this.zhi_xiang = zhi_xiang;
    }

    public ZhangWenLeiXingBean getZhang_wen_lei_xing() {
        return zhang_wen_lei_xing;
    }

    public void setZhang_wen_lei_xing(ZhangWenLeiXingBean zhang_wen_lei_xing) {
        this.zhang_wen_lei_xing = zhang_wen_lei_xing;
    }

    public ZhangWenBean getZhang_wen() {
        return zhang_wen;
    }

    public void setZhang_wen(ZhangWenBean zhang_wen) {
        this.zhang_wen = zhang_wen;
    }

    public ScoreBean getScore() {
        return score;
    }

    public void setScore(ScoreBean score) {
        this.score = score;
    }

    public TagBean getTag() {
        return tag;
    }

    public void setTag(TagBean tag) {
        this.tag = tag;
    }

    public static class UserInfoBean {
        /**
         * name :
         * ucode :
         * direct : 0
         */

        private String name;
        private String ucode;
        private int direct;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUcode() {
            return ucode;
        }

        public void setUcode(String ucode) {
            this.ucode = ucode;
        }

        public int getDirect() {
            return direct;
        }

        public void setDirect(int direct) {
            this.direct = direct;
        }
    }

    public static class ShouXiangBean {
        /**
         * title : 手相分析
         * tag : 金形手
         * intro : 手相有阴阳男女之分，大小之别，手形之论，金木水火土是手形的基本分法。手形代表着人的吉凶富贵。
         * dec : ["你的手形为金形手，金形手属于理性，工作能力强，有领导能力的手形，有此手形的男女，崇尚实际而不慕浮华，谨守秩序，意志坚定，忍耐力强，洁身自爱，能俭扑持家而不耽安逸，是务实派，能成大业且长寿，但欠圆滑，固执己见，并有过份吝啬的倾向，而无豪爽之气。 "]
         * sub_dec : []
         */

        private String title;
        private String tag;
        private String intro;
        private List<String> dec;
        private List<?> sub_dec;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public String getIntro() {
            return intro;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public List<String> getDec() {
            return dec;
        }

        public void setDec(List<String> dec) {
            this.dec = dec;
        }

        public List<?> getSub_dec() {
            return sub_dec;
        }

        public void setSub_dec(List<?> sub_dec) {
            this.sub_dec = sub_dec;
        }
    }

    public static class PingFenBean {
        /**
         * LL_Score : 81
         * WL_Score : 74
         * AL_Score : 84
         * BL_Score : 60
         * fingerScore : 92
         */

        private int LL_Score;
        private int WL_Score;
        private int AL_Score;
        private int BL_Score;
        private int fingerScore;

        public int getLL_Score() {
            return LL_Score;
        }

        public void setLL_Score(int LL_Score) {
            this.LL_Score = LL_Score;
        }

        public int getWL_Score() {
            return WL_Score;
        }

        public void setWL_Score(int WL_Score) {
            this.WL_Score = WL_Score;
        }

        public int getAL_Score() {
            return AL_Score;
        }

        public void setAL_Score(int AL_Score) {
            this.AL_Score = AL_Score;
        }

        public int getBL_Score() {
            return BL_Score;
        }

        public void setBL_Score(int BL_Score) {
            this.BL_Score = BL_Score;
        }

        public int getFingerScore() {
            return fingerScore;
        }

        public void setFingerScore(int fingerScore) {
            this.fingerScore = fingerScore;
        }
    }

    public static class ZhiXiangBean {
        /**
         * title : 指相分析
         * dec : [{"title":"有支配欲","dec":["你的食指比较长，代表你的性格也比较顽强，态度强硬，做事做人目的性很强，有理想有追求。","但在花钱方面可能会表现得很大方，财大气粗的，导致很难存得住钱，当然并不是说你的赚钱能力不强，只是赚钱多，花钱也多，需要加强理财，控制一下自己的欲望和消费习惯，才能存得住钱。"],"img":"https://img-fe.ggwan.com/images/82516ca217f7f0-164x163.png","name":"shi_zhi"},{"title":"运势较好","dec":["你的中指比较长，代表你的先天运况比较好，但也代表你会为人比较以自我为中心，做事作风全看自己的喜好，希望别人按照自己的想法来做事，给人很强势的感觉。","对与错区别在于你是帮人还是害人，所以，你需要多读书学习，提升自身的知识面和判断力，相信你能成为同事朋友眼中的军师高人，获得人缘助力。"],"img":"https://img-fe.ggwan.com/images/5dda8fbf8e8beb-164x163.png","name":"zhong_zhi"},{"title":"恋情不顺","img":"https://img-fe.ggwan.com/images/b2beb4f3a26a39-163x163.png","name":"wu_ming_zhi"},{"title":"子女惹祸","img":"https://img-fe.ggwan.com/images/d928215d854685-174x163.png","name":"xiao_zhi"}]
         */

        private String title;
        private List<DecBean> dec;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<DecBean> getDec() {
            return dec;
        }

        public void setDec(List<DecBean> dec) {
            this.dec = dec;
        }

        public static class DecBean {
            /**
             * title : 有支配欲
             * dec : ["你的食指比较长，代表你的性格也比较顽强，态度强硬，做事做人目的性很强，有理想有追求。","但在花钱方面可能会表现得很大方，财大气粗的，导致很难存得住钱，当然并不是说你的赚钱能力不强，只是赚钱多，花钱也多，需要加强理财，控制一下自己的欲望和消费习惯，才能存得住钱。"]
             * img : https://img-fe.ggwan.com/images/82516ca217f7f0-164x163.png
             * name : shi_zhi
             */

            private String title;
            private String img;
            private String name;
            private List<String> dec;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public List<String> getDec() {
                return dec;
            }

            public void setDec(List<String> dec) {
                this.dec = dec;
            }
        }
    }


    public static class ZhangWenLeiXingBean {
        /**
         * title : 掌纹类型分析
         */

        private String title;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }


    public static class ZhangWenBean {
        /**
         * title : 掌纹分析
         * dec : [{"title":"为情所累","img":"https://img-fe.ggwan.com/images/53289a1d5be3a1-170x163.png","name":"gan_qing"},{"title":"能力出众","img":"https://img-fe.ggwan.com/images/5da599c74baeef-171x165.png","name":"zhi_hui"},{"title":"不强求","img":"https://img-fe.ggwan.com/images/d3dcdca5573be7-170x163.png","name":"shi_ye"},{"title":"精力旺盛","img":"https://img-fe.ggwan.com/images/b0d6f320832726-170x163.png","name":"sheng_ming"}]
         */

        private String title;
        private List<DecBeanX> dec;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<DecBeanX> getDec() {
            return dec;
        }

        public void setDec(List<DecBeanX> dec) {
            this.dec = dec;
        }

        public static class DecBeanX {
            /**
             * title : 为情所累
             * img : https://img-fe.ggwan.com/images/53289a1d5be3a1-170x163.png
             * name : gan_qing
             */

            private String title;
            private String img;
            private String name;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }


    public static class ScoreBean {
        /**
         * title : 手相分数分析
         * score : 88
         * level : 很好
         */

        private String title;
        private int score;
        private String level;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }
    }


    public static class TagBean {
        /**
         * title : 手相标签分析
         * dec : {"indexMiddle":{"score":5,"tag":"有支配欲"},"middle":{"score":5,"tag":"运势较好"},"ringMiddle":{"score":5,"tag":"恋情甜蜜"},"ringIndex":{"score":3,"tag":"恋情不顺"},"littleRing":{"score":3,"tag":"子女惹祸"},"hunYin":{"score":9,"tag":"异性缘少"},"zhiHui":{"score":18,"tag":"能力出众"},"shiYe":{"score":14,"tag":"不强求"},"shengMing":{"score":10,"tag":"精力旺盛"},"ganQing":{"score":17,"tag":"为情所累"},"zhangWen":{"score":8,"tag":"思维缜密"}}
         */

        private String title;
        private DecBeanXX dec;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public DecBeanXX getDec() {
            return dec;
        }

        public void setDec(DecBeanXX dec) {
            this.dec = dec;
        }

        public static class DecBeanXX {
            /**
             * indexMiddle : {"score":5,"tag":"有支配欲"}
             * middle : {"score":5,"tag":"运势较好"}
             * ringMiddle : {"score":5,"tag":"恋情甜蜜"}
             * ringIndex : {"score":3,"tag":"恋情不顺"}
             * littleRing : {"score":3,"tag":"子女惹祸"}
             * hunYin : {"score":9,"tag":"异性缘少"}
             * zhiHui : {"score":18,"tag":"能力出众"}
             * shiYe : {"score":14,"tag":"不强求"}
             * shengMing : {"score":10,"tag":"精力旺盛"}
             * ganQing : {"score":17,"tag":"为情所累"}
             * zhangWen : {"score":8,"tag":"思维缜密"}
             */

            private IndexMiddleBean indexMiddle;
            private MiddleBean middle;
            private RingMiddleBean ringMiddle;
            private RingIndexBean ringIndex;
            private LittleRingBean littleRing;
            private HunYinBean hunYin;
            private ZhiHuiBean zhiHui;
            private ShiYeBean shiYe;
            private ShengMingBean shengMing;
            private GanQingBean ganQing;
            private ZhangWenBeanX zhangWen;

            public IndexMiddleBean getIndexMiddle() {
                return indexMiddle;
            }

            public void setIndexMiddle(IndexMiddleBean indexMiddle) {
                this.indexMiddle = indexMiddle;
            }

            public MiddleBean getMiddle() {
                return middle;
            }

            public void setMiddle(MiddleBean middle) {
                this.middle = middle;
            }

            public RingMiddleBean getRingMiddle() {
                return ringMiddle;
            }

            public void setRingMiddle(RingMiddleBean ringMiddle) {
                this.ringMiddle = ringMiddle;
            }

            public RingIndexBean getRingIndex() {
                return ringIndex;
            }

            public void setRingIndex(RingIndexBean ringIndex) {
                this.ringIndex = ringIndex;
            }

            public LittleRingBean getLittleRing() {
                return littleRing;
            }

            public void setLittleRing(LittleRingBean littleRing) {
                this.littleRing = littleRing;
            }

            public HunYinBean getHunYin() {
                return hunYin;
            }

            public void setHunYin(HunYinBean hunYin) {
                this.hunYin = hunYin;
            }

            public ZhiHuiBean getZhiHui() {
                return zhiHui;
            }

            public void setZhiHui(ZhiHuiBean zhiHui) {
                this.zhiHui = zhiHui;
            }

            public ShiYeBean getShiYe() {
                return shiYe;
            }

            public void setShiYe(ShiYeBean shiYe) {
                this.shiYe = shiYe;
            }

            public ShengMingBean getShengMing() {
                return shengMing;
            }

            public void setShengMing(ShengMingBean shengMing) {
                this.shengMing = shengMing;
            }

            public GanQingBean getGanQing() {
                return ganQing;
            }

            public void setGanQing(GanQingBean ganQing) {
                this.ganQing = ganQing;
            }

            public ZhangWenBeanX getZhangWen() {
                return zhangWen;
            }

            public void setZhangWen(ZhangWenBeanX zhangWen) {
                this.zhangWen = zhangWen;
            }

            public static class IndexMiddleBean {
                /**
                 * score : 5
                 * tag : 有支配欲
                 */

                private int score;
                private String tag;
            }


            public static class MiddleBean {
                /**
                 * score : 5
                 * tag : 运势较好
                 */

                private int score;
                private String tag;
            }


            public static class RingMiddleBean {
                /**
                 * score : 5
                 * tag : 恋情甜蜜
                 */

                private int score;
                private String tag;
            }


            public static class RingIndexBean {
                /**
                 * score : 3
                 * tag : 恋情不顺
                 */

                private int score;
                private String tag;
            }


            public static class LittleRingBean {
                /**
                 * score : 3
                 * tag : 子女惹祸
                 */

                private int score;
                private String tag;
            }


            public static class HunYinBean {
                /**
                 * score : 9
                 * tag : 异性缘少
                 */

                private int score;
                private String tag;
            }


            public static class ZhiHuiBean {
                /**
                 * score : 18
                 * tag : 能力出众
                 */

                private int score;
                private String tag;
            }


            public static class ShiYeBean {
                /**
                 * score : 14
                 * tag : 不强求
                 */

                private int score;
                private String tag;
            }


            public static class ShengMingBean {
                /**
                 * score : 10
                 * tag : 精力旺盛
                 */

                private int score;
                private String tag;
            }


            public static class GanQingBean {
                /**
                 * score : 17
                 * tag : 为情所累
                 */

                private int score;
                private String tag;
            }


            public static class ZhangWenBeanX {
                /**
                 * score : 8
                 * tag : 思维缜密
                 */

                private int score;
                private String tag;
            }
        }
    }
}

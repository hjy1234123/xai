package cn.ydxiaoshuai.common.api.vo.config;

import cn.ydxiaoshuai.common.api.vo.BaseResponseBean;
import cn.ydxiaoshuai.common.constant.CommonConstant;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @author 小帅丶
 * @className IsmIndexSlideResult
 * @Description 刷新文案对象
 * @Date 2020年5月14日15:07:08
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiteConfigCopywriteResult extends BaseResponseBean {
    /**
     * 具体参数
     **/
    private Data data;
    @lombok.Data
    public static class Data{
        /** 上刷新内容 */
        private String aboveText;
        /** 下刷新内容 */
        private String belowText;
    }
    public LiteConfigCopywriteResult success(String message, Data data) {
        this.message = message;
        this.code = CommonConstant.SC_OK_200;
        this.data = data;
        return this;
    }
    public LiteConfigCopywriteResult fail(String message, Integer code) {
        this.message = message;
        this.code = code;
        return this;
    }
    public LiteConfigCopywriteResult error(String message) {
        this.message = message;
        this.code = CommonConstant.SC_INTERNAL_SERVER_ERROR_500;
        return this;
    }
}

package cn.ydxiaoshuai.common.system.vo;

import cn.ydxiaoshuai.common.api.vo.BaseResponseBean;
import cn.ydxiaoshuai.common.constant.CommonConstant;
import lombok.Data;

/**
 * @author 小帅丶
 * @className DelOssResponseBean
 * @Description 删除返回的bean
 * @Date 2020/9/4-14:48
 **/
@Data
public class DelOssResponseBean extends BaseResponseBean {

    public DelOssResponseBean success(String message,String message_zh) {
        this.message = message;
        this.message_zh = message_zh;
        this.code = CommonConstant.SC_OK_200;
        return this;
    }

    public DelOssResponseBean fail(String message,String message_zh,Integer code) {
        this.message = message;
        this.message_zh = message_zh;
        this.code = code;
        return this;
    }

    public DelOssResponseBean error(String message,String message_zh) {
        this.message = message;
        this.message_zh = message_zh;
        this.code = CommonConstant.SC_INTERNAL_SERVER_ERROR_500;
        return this;
    }
}

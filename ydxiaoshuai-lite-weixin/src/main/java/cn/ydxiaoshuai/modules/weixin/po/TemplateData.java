package cn.ydxiaoshuai.modules.weixin.po;

import lombok.Data;

/**
 * @Description 模板内容
 * @author 小帅丶
 * @className TemplateData
 * @Date 2019/11/27-14:13
 **/
@Data
public class TemplateData {
    /** 模板内容 */
    private String value;
    /** 模板字体颜色 不填默认黑色 在PC此字段不生效 */
    private String color;
}

package cn.ydxiaoshuai.modules.logoexamine.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.ydxiaoshuai.modules.logoexamine.entity.LogoExamineRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: LOGO自定义上传审核记录表
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface LogoExamineRecordMapper extends BaseMapper<LogoExamineRecord> {

}

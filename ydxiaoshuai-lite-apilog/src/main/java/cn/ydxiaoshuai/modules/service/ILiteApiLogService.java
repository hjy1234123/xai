package cn.ydxiaoshuai.modules.service;

import cn.ydxiaoshuai.modules.entity.LiteApiLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: API日志记录表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
public interface ILiteApiLogService extends IService<LiteApiLog> {

}
